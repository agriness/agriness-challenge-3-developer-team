# Agriness - Challenge 3 - Developer Team

## Introduction

Welcome to the Agriness Challenge.

This challenge is only meant to understand how you are going to use some technical concepts currently used for us.

We will now describe some requirements and you should resolve using the technologies of your choice. But, we recommend the technologies of our stack (Python, Django, Flask, PostgreSQL, Django Rest Framework, Swagger...).

## A Simple Content Management System (CMS)

#### Domain:

- Article: is a simple page with a title whose content is written in the format: plain text, HTML or Markdown; In addition, every article has an author and has "published" or "draft" status.

- Author: is a person with name and email.

- Category: Every article has one or more categories.

#### Features:

- Timeline (with pagination): it should be possible to request a list of articles in reverse chronological order (date of publication);

- Filter by author: it should be possible to request articles from a specific author;

- Filters by category: it should be possible to request articles in a specific category;

- Detail of an article: it should be possible to request a specific article along with its details (author, category and other attributes);

- Search: it should be possible to search on the title and body of the article (for example: "?search=brazil" should return all articles that have the term "brazil" in the title and/or body).

#### Other requirements:

- We need an API service;
- We need a online documentation about this API (for example, Swagger);

## Notes About This Challenge:

- Document your project (README) and make sure other developers can easily run the application locally.
- Automated tests are mandatory.
- When you're done, share with us the GIT repository (Bitbucket or GitHub). Make sure we have full access to it.
- Don't worry about authentication or security at this time.